# hello-express

This project contains REST API with folloeing features:

- Welcome endpoint
- Multiply endpoint

## Getting started

First install the dependencies

```sh
npm i
```

Start the REST API service:

`node app.js`

Or in the latest NodeJS version (>=20):

`node --watch app.js`

